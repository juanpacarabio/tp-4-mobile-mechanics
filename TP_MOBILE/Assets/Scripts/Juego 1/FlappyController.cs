using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FlappyController : MonoBehaviour
{

    [Header("-----Jugador-----\n")]
    private Rigidbody rb;
    public float FuerzaSalto;
    [SerializeField] private float tiempoRecorrido;
    [Header("-----Parallax-----\n")]
    public ParallaxObstaculos[] parallax;
    [Header("-----UI-----\n")]
    [SerializeField] private TMPro.TMP_Text gameOverHUD;
    [SerializeField] private TMPro.TMP_Text Contador;


    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        tiempoRecorrido = 0;

    }

    void Update()
    {
        Saltar();
        Tiempo();
        ResetearEscena();
        VolverMenu();
    }

    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            rb.AddForce(new Vector3(0,FuerzaSalto,0),ForceMode.Impulse);
        }
    }

    private void ResetearEscena()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
            rb.constraints = RigidbodyConstraints.FreezePositionX |RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;  
        }
    }

    private void VolverMenu()
    {
        if (Input.GetKeyDown(KeyCode.P))
            SceneManager.LoadScene(0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        for (int i = 0; i < parallax.Length; i++)
        {
            parallax[i].GetComponent<ParallaxObstaculos>().enabled = false;
        }
        gameOverHUD.gameObject.SetActive(true);
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    private void Tiempo()
    {
        if (gameOverHUD.gameObject.activeInHierarchy==false)
        {
            tiempoRecorrido = tiempoRecorrido + 1 * Time.deltaTime;
            Contador.text = tiempoRecorrido.ToString("0");
        }
    }

}
