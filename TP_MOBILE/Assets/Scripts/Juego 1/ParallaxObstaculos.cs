using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxObstaculos : MonoBehaviour
{
    public GameObject cam;
    public float EndPoint;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        startPos = transform.position.z;
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            transform.position = new Vector3(transform.position.x , transform.position.y, transform.position.z - parallaxEffect);
            if (transform.localPosition.z < -16.9f)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, EndPoint);
            }
        }

    }
}
