using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    [SerializeField] private float velocidad;
    [SerializeField] private Rigidbody proyectil;
    [SerializeField] private GameObject Esfera;
    public float balasTotales;
    [SerializeField] private float velocidadRotacion;

    void Start()
    {
        balasTotales=0;
        Esfera = GameObject.Find("Sphere");
    }

    void Update()
    {
        Shoot();
        RotarEsfera();
        //if (rotarTarget)
        //{
        //    Esfera.transform.rotation = Quaternion.Inverse(Esfera.transform.rotation);
        //    Esfera.transform.Rotate(Vector3.up, 50f * Time.deltaTime);
        //}
    }

    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            var clone = Instantiate(proyectil, transform.position, transform.rotation);
            clone.velocity = Vector3.right * velocidad;
            balasTotales += 1;
        }
    }

    private void RotarEsfera()
    {
        //if (Input.GetKeyDown(KeyCode.Mouse1) && rotarTarget==false)
        //{
        //    rotarTarget = true;
        //}
       

    }
}
