using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Proyectil : MonoBehaviour
{
    [SerializeField] private Rotador esfera;
    [SerializeField] private Disparar player;
    [SerializeField] private J2_UI_Manager UI_Manager;

    private void Awake()
    {
        GameObject planeta = GameObject.Find("Sphere");
        esfera = planeta.GetComponent<Rotador>();
        GameObject cubo = GameObject.Find("Cube");
        player = cubo.GetComponent<Disparar>();
        GameObject UI = GameObject.Find("UI Manager");
        UI_Manager = UI.GetComponent<J2_UI_Manager>();
    }
    private void Start()
    {
        
    }

    private void Update()
    {
        ResetearPartida();
        VolverMenu();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Esfera"))
        {
            gameObject.transform.parent = collision.gameObject.transform;
        }
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            GameOver();
        }

    }

    private void GameOver()
    {
        UI_Manager.Derrota= true;
        esfera.GetComponent<Rotador>().enabled = false;
        player.GetComponent<Disparar>().enabled = false;
        GameObject[] balas = GameObject.FindGameObjectsWithTag("Proyectil");
        foreach (GameObject b in balas)
        {
            Rigidbody rb = b.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }

    }


    private void ResetearPartida()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            UI_Manager.Derrota = false;
            player.balasTotales = 0;
            esfera.GetComponent<Rotador>().enabled = true;
            player.GetComponent<Disparar>().enabled = true;
            GameObject[] balas = GameObject.FindGameObjectsWithTag("Proyectil");
            foreach (GameObject b in balas)
            {
                Rigidbody rb = b.GetComponent<Rigidbody>();
                rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ ;
                Destroy(b);
            }

        }
        
    }

    private void VolverMenu()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            player.balasTotales = 0;
            SceneManager.LoadScene(0);
        }
    }


}
