using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class J2_UI_Manager : MonoBehaviour
{
    public TMPro.TMP_Text GameOver;
    public bool Derrota;
    [SerializeField] private TMPro.TMP_Text bullets;
    [SerializeField] private Disparar shoot;

    private void Awake()
    {
        //if (instance == null)
        //{
        //    instance = this;
        //}
        //else if (instance != this)
        //{
        //    Destroy(gameObject);
        //}
        //DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Derrota = false;
        GameObject cube = GameObject.Find("Cube");
        shoot = cube.GetComponent<Disparar>();
        
    }

    void Update()
    {
        CondicionDerrota();
        MostrarBalas();
    }


    private void CondicionDerrota()
    {
        if (Derrota == true)
        {
            GameOver.gameObject.SetActive(true);
        }
        else 
        {
            GameOver.gameObject.SetActive(false);

        }
    }

    private void MostrarBalas()
    {
        bullets.text = shoot.balasTotales.ToString();
    }
}
