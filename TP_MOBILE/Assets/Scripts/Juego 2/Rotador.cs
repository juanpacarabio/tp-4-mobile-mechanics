using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    [SerializeField] private Transform Planeta;
    [SerializeField] private float rotationSpeed = 50f;
    [SerializeField] private Disparar shoot;
    
    void Update()
    {

        Planeta.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
        
    }

}
