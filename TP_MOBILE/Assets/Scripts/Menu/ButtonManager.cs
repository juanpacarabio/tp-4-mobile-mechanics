using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public GameObject[] menus;
    void Start()
    {
        MenuPrincipal();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void MenuPrincipal()
    {
        menus[0].SetActive(true);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void ChooseGame()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(true);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void Creditos()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(true);

    }
    public void ControlesJ1()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(true);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void ControlesJ2()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(true);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void ControlesJ3()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(true);
        menus[5].SetActive(false);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void ControlesJ4()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(true);
        menus[6].SetActive(false);
        menus[7].SetActive(false);

    }
    public void ControlesJ5()
    {
        menus[0].SetActive(false);
        menus[1].SetActive(false);
        menus[2].SetActive(false);
        menus[3].SetActive(false);
        menus[4].SetActive(false);
        menus[5].SetActive(false);
        menus[6].SetActive(true);
        menus[7].SetActive(false);

    }
    public void PlayJ1()
    {
        SceneManager.LoadScene(1);
    }
    public void PlayJ2()
    {
        SceneManager.LoadScene(2);
    }
    public void PlayJ3()
    {
        SceneManager.LoadScene(3);
    }
    public void PlayJ4()
    {
        SceneManager.LoadScene(4);
    }
    public void PlayJ5()
    {
        SceneManager.LoadScene(5);
    }
    public void Salir()
    {
        Application.Quit();
    }


}
