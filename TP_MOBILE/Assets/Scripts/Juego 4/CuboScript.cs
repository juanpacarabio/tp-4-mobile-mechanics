using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboScript : MonoBehaviour
{
    public bool tocadoON;
    bool invertido;
    public GameObject cubo;
    Rigidbody rb;
    public Vector3 gravedad;
    public Material materialON;
    public Material materialOFF;
    public GameObject Manager;
    void Start()
    {
        rb = cubo.GetComponent<Rigidbody>();
        gravedad = Physics.gravity;
    }

    private void FixedUpdate()
    {
        if (invertido == true)
        {
            rb.AddForce(-gravedad, ForceMode.Acceleration);
        }
    }

    public void InvertirGravedad()
    {
        tocadoON = !tocadoON;

        if (tocadoON == true)
        {
            cubo.GetComponent<Renderer>().material = materialON;
            rb.useGravity = false; 
            invertido = true;
        }
        if (tocadoON == false)
        {
            cubo.GetComponent<Renderer>().material = materialOFF;
            rb.useGravity = true;
            invertido = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Objetivo"))
        {
            if (tocadoON == false)
            {
                cubo.tag = "Objetivo";
                CuboManager script = Manager.GetComponent<CuboManager>();
                script.cantidadcubos++;
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Objetivo"))
        {
            cubo.tag = "Player";
            CuboManager script = Manager.GetComponent<CuboManager>();
            script.cantidadcubos--;
        }
    }
}
