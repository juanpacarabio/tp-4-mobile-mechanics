using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataformas : MonoBehaviour
{
    public float velocidad;
    Vector3 direccion = new Vector3();
    void Start()
    {
        
    }


    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            direccion = new Vector3(0f, 0f, 1f);
            transform.Rotate(velocidad * direccion * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            direccion = new Vector3(0f, 0f, -1f);
            transform.Rotate(velocidad * direccion * Time.deltaTime);
        }
    }
}
