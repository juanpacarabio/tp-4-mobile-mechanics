using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstraintsCamara : MonoBehaviour
{
    Quaternion rotacion;
    void Start()
    {
        rotacion = this.transform.rotation; 
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.rotation = rotacion;
    }
}
