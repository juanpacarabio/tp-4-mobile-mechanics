using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerPelota : GameManager
{
    public GameObject pelota;
    Vector3 posicioninicial;
    GameObject[] plataformas;
    [SerializeField] private GameObject victoriaHUD;
    void Start()
    {
        posicioninicial = pelota.transform.position;
        plataformas = GameObject.FindGameObjectsWithTag("Plataforma");
        victoriaHUD.SetActive(false);
    }

    
    void Update()
    {
        if (pelota.transform.position.y < -10)
        {
            Restart();
        }
        if (Input.GetKey(KeyCode.R))
        {
            Restart();
        }
        VolverMenu();
    }

    public void Victoria()
    {
        Debug.Log("Ganaste!");
        victoriaHUD.SetActive(true);
        Time.timeScale = 0;
    }

    public void Restart()
    {
        Time.timeScale = 1;
        victoriaHUD.SetActive(false);
        Debug.Log("Restart!");
        foreach(GameObject plataforma in plataformas)
        {
            plataforma.transform.rotation = Quaternion.identity;
        }
        pelota.transform.position = posicioninicial;
        pelota.GetComponent<Rigidbody>().velocity = Vector3.zero;
        pelota.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void VolverMenu()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
            
    }

}
