using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaDoble : MalabaresScript
{
    public GameObject pelota1;
    public GameObject pelota2;
    bool tocado = false;
    private void Update()
    {
        if (tocado == true)
        {
            pelota.GetComponent<SphereCollider>().enabled = false;
            pelota.GetComponent<MeshRenderer>().enabled = false;
            pelota1.SetActive(true);
            pelota2.SetActive(true);

            pelota1.transform.parent = null;
            pelota2.transform.parent = null;


            MalabaresScript empujar1 = pelota1.GetComponent<MalabaresScript>();
            empujar1.Empujar();
            MalabaresScript empujar2 = pelota2.GetComponent<MalabaresScript>();
            empujar2.Empujar();

            Destroy(pelota);
        }
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        if (collision.gameObject.CompareTag("Player"))
        {
            Separarse();
        }
    }

    void Separarse()
    {
        tocado = true;
    }
}
