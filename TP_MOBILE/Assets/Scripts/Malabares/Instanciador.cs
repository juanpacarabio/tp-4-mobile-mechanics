using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciador : MonoBehaviour
{
    public GameObject pelotaBase;
    public GameObject pelotaDoble;
    public GameObject[] coordenadas;
    int coord_actual = 0;
    public float rapidez;
    float coor_espacio = 2;
    bool instanciando = true;
    public GameObject gameManager;
    MalabaresManager manager;
    public int pelotas;
    public float tiempoentreinstancias;

    void Start()
    {
        ComenzarInstancias();
        gameManager = GameObject.Find("GameManager");
        manager = gameManager.GetComponent<MalabaresManager>();
    }

    // Update is called once per frame
    void Update()
    {
        manager.score = pelotas;
        if (Vector3.Distance(coordenadas[coord_actual].transform.position, transform.position) < coor_espacio) //si la distancia entre la coordenada y el objeto es menor al espacio que compone el punto
        {
            coord_actual++;
            if (coord_actual >= coordenadas.Length)
            {
                coord_actual = 0;
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, coordenadas[coord_actual].transform.position, Time.deltaTime * rapidez);

        GameObject[] bolas = GameObject.FindGameObjectsWithTag("Player");

        if (bolas == null)
        {
            tiempoentreinstancias = 0;
        }
        else
        {
            tiempoentreinstancias = 10;
        }
    }


    public void ComenzarInstancias()
    {
        StartCoroutine(Instanciar());
    }


    GameObject proximoInstanciado;
    public IEnumerator Instanciar()
    {
        while (instanciando == true)
        {
            GameObject pelotaNueva;
            int random = Random.Range(0, 10);
            if (random <= 7)
            {
                proximoInstanciado = pelotaBase;
            }else if (random > 7)
            {
                proximoInstanciado = pelotaDoble;
            }
            pelotaNueva = Instantiate(proximoInstanciado, transform.position, transform.rotation);
            pelotas++;
            
            yield return new WaitForSeconds(tiempoentreinstancias);
        }
    }
}
