using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MalabaresManager : GameManager
{
    GameObject objetoTocado;
    public int vidas;
    public int cantidadPelotas;
    private UIManager_Malabares UI_Manager;
    GameObject instanciador;
    [SerializeField] private TMPro.TMP_Text contadorVidas;
    [SerializeField] private TMPro.TMP_Text textoGeneral;


    void Start()
    {
        vidas = 3;
        UI_Manager = UIManager_Malabares.instance;
        instanciador = GameObject.Find("Instantiator");
        StartCoroutine(TextoGone());
    }

    void Update()
    {
        ResetearPartida();
        HUDVidas();
        Debug.Log("Puntuación: " + score);
        if (vidas <= 0)
        {
            Perder();
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                objetoTocado = hit.transform.gameObject;
                MalabaresScript pelota = objetoTocado.GetComponent<MalabaresScript>();
                pelota.Empujar();
            }
        }
    }

    public void Perder()
    {
        Debug.Log("Perdistes!");
        textoGeneral.gameObject.SetActive(true);
        textoGeneral.text = "Game Over.\n R para \n reintentar.";
        UI_Manager.Derrota = true;
        instanciador.SetActive(false);

        GameObject[] bolas = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject b in bolas)
        {
            Rigidbody rb = b.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }

    }

    private void HUDVidas()
    {
        contadorVidas.text = vidas.ToString();
    }

    private void ResetearPartida()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
            //UI_Manager.Derrota = false;
            //instanciador.SetActive(true);
            //GameObject[] bolas = GameObject.FindGameObjectsWithTag("Player");
            //foreach (GameObject b in bolas)
            //{
            //    Destroy(b);
            //}
            //score = 0;
            //cantidadPelotas = 0;
            //vidas = 3;
            //instanciador.GetComponent<Instanciador>().pelotas = 0;
            //instanciador.GetComponent<Instanciador>().ComenzarInstancias();
            //instanciador.GetComponent<Instanciador>().tiempoentreinstancias = 0;
        }
    }

    IEnumerator TextoGone()
    {
        textoGeneral.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(7f);
        textoGeneral.gameObject.SetActive(false);
        yield break;
    }

}
