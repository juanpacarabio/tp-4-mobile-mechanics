using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MalabaresScript : MonoBehaviour
{
    public GameObject pelota;
    Rigidbody rb;
    public float fuerza = 800f;
    public GameObject gameManager;
    public void Start()
    {
        rb = pelota.GetComponent<Rigidbody>();
        gameManager = GameObject.Find("GameManager");
    }
    public void Empujar()
    {
        Vector3 temp = transform.rotation.eulerAngles;

        int direccion = Random.Range(0, 31);
        if (direccion < 20)
        {
            temp.z = -20;
        }else if (direccion >= 20)
        {
            temp.z = 20;
        }
        transform.rotation = Quaternion.Euler(temp);
        pelota.GetComponent<Rigidbody>().velocity = Vector3.zero;
        pelota.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        rb.AddForce(transform.up * fuerza);
    }

     protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            MalabaresManager manager = gameManager.GetComponent<MalabaresManager>();
            manager.vidas = manager.vidas - 1;
            Destroy(pelota);
        }
    }
}
